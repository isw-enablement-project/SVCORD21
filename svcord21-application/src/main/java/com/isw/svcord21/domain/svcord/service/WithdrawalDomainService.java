package com.isw.svcord21.domain.svcord.service;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cloud.sleuth.annotation.NewSpan;
import org.springframework.stereotype.Service;

import com.isw.svcord21.sdk.domain.svcord.entity.CreateServicingOrderProducerInput;
import com.isw.svcord21.sdk.domain.svcord.entity.CustomerReferenceEntity;
import com.isw.svcord21.sdk.domain.svcord.entity.Svcord21;
import com.isw.svcord21.sdk.domain.svcord.entity.UpdateServicingOrderProducerInput;
import com.isw.svcord21.sdk.domain.svcord.entity.WithdrawalDomainServiceOutput;
import com.isw.svcord21.sdk.domain.svcord.service.WithdrawalDomainServiceBase;
import com.isw.svcord21.sdk.domain.svcord.type.ServicingOrderWorkResult;
import com.isw.svcord21.sdk.integration.facade.IntegrationEntityBuilder;
import com.isw.svcord21.sdk.integration.partylife.entity.RetrieveLoginInput;
import com.isw.svcord21.sdk.integration.partylife.entity.RetrieveLoginOutput;
import com.isw.svcord21.sdk.integration.paymord.entity.PaymentOrderInput;
import com.isw.svcord21.sdk.integration.paymord.entity.PaymentOrderOutput;
import com.isw.svcord21.domain.svcord.command.Svcord21Command;
import com.isw.svcord21.integration.partylife.service.RetrieveLogin;
import com.isw.svcord21.integration.paymord.service.PaymentOrder;
import com.isw.svcord21.sdk.api.v1.model.CustomerReference;
import com.isw.svcord21.sdk.domain.facade.DomainEntityBuilder;
import com.isw.svcord21.sdk.domain.facade.Repository;

@Service
public class WithdrawalDomainService extends WithdrawalDomainServiceBase {

  private static final Logger log = LoggerFactory.getLogger(WithdrawalDomainService.class);

  @Autowired
  Svcord21Command servicingOrderProcedureCommand;

  @Autowired
  PaymentOrder paymentOrder;

  @Autowired
  RetrieveLogin retrieveLogin;

  @Autowired
  IntegrationEntityBuilder integrationEntityBuilder;

  public WithdrawalDomainService(DomainEntityBuilder entityBuilder,  Repository repo) {
  
    super(entityBuilder,  repo);
    
  }
  
  @NewSpan
  @Override
  public com.isw.svcord21.sdk.domain.svcord.entity.WithdrawalDomainServiceOutput execute(com.isw.svcord21.sdk.domain.svcord.entity.WithdrawalDomainServiceInput withdrawalDomainServiceInput)  {

    log.info("WithdrawalDomainService.execute()");
    // TODO: Add your service implementation logic
   
    // 사용자 인증요청 Party Life Cycle Management 호출
    RetrieveLoginInput retrieveInput = integrationEntityBuilder
    .getPartylife()
    .getRetrieveLoginInput()
    .setId("test1")
    .build();

    RetrieveLoginOutput retrieveOutput = retrieveLogin.execute(retrieveInput);

    // User Valid 체크
    if (! retrieveOutput.getResult().equals("SUCCESS"))  return null;

    // Root Entity Servicing Order 생성
    CustomerReferenceEntity customerReference = this.entityBuilder.getSvcord().getCustomerReference().build();
    customerReference.setAccountNumber(withdrawalDomainServiceInput.getAccountNumber());
    customerReference.setAmount(withdrawalDomainServiceInput.getAmount());

    CreateServicingOrderProducerInput createInput =  this.entityBuilder.getSvcord().getCreateServicingOrderProducerInput().build();
    
    createInput.setCustomerReference(customerReference);
    Svcord21 createOuput = servicingOrderProcedureCommand.createServicingOrderProducer(createInput);

    // Payment Order 호출
    PaymentOrderInput paymentInput = integrationEntityBuilder.getPaymord().getPaymentOrderInput().build();
    paymentInput.setAccountNumber(withdrawalDomainServiceInput.getAccountNumber());
    paymentInput.setAmount(withdrawalDomainServiceInput.getAmount());
    paymentInput.setExternalId("SVCORD21");
    paymentInput.setExternalSerive("SVCORD21");
    paymentInput.setPaymentType("CASH_WITHDRAWAL");

    PaymentOrderOutput paymentOutput =  paymentOrder.execute(paymentInput);

    // DB update
    UpdateServicingOrderProducerInput updateInput = this.entityBuilder.getSvcord().getUpdateServicingOrderProducerInput().build();
    updateInput.setUpdateID(createOuput.getId().toString());
    updateInput.setServicingOrderWorkResult(ServicingOrderWorkResult.valueOf(paymentOutput.getPaymentOrderResult()));

    servicingOrderProcedureCommand.updateServicingOrderProducer(createOuput, updateInput);

    // 결과값 전달
    WithdrawalDomainServiceOutput withdrawalOutput = this.entityBuilder.getSvcord().getWithdrawalDomainServiceOutput().build();
    withdrawalOutput.setServicingOrderWorkResult(ServicingOrderWorkResult.valueOf(paymentOutput.getPaymentOrderResult()));
    withdrawalOutput.setTrasactionId(paymentOutput.getTransactionId());
    return withdrawalOutput;
  }

}
