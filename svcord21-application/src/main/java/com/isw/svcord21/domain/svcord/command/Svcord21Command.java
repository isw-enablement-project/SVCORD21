package com.isw.svcord21.domain.svcord.command;


import org.springframework.stereotype.Service;
import com.isw.svcord21.sdk.domain.svcord.command.Svcord21CommandBase;
import com.isw.svcord21.sdk.domain.svcord.entity.Svcord21Entity;
import com.isw.svcord21.sdk.domain.svcord.entity.ThirdPartyReferenceEntity;
import com.isw.svcord21.sdk.domain.svcord.type.ServicingOrderType;
import com.isw.svcord21.sdk.domain.svcord.type.ServicingOrderWorkProduct;
import com.isw.svcord21.sdk.domain.svcord.type.ServicingOrderWorkResult;
import com.isw.svcord21.sdk.domain.facade.DomainEntityBuilder;
import com.isw.svcord21.sdk.domain.facade.Repository;

import java.time.LocalDate;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

@Service
public class Svcord21Command extends Svcord21CommandBase {

  private static final Logger log = LoggerFactory.getLogger(Svcord21Command.class);

  public Svcord21Command(DomainEntityBuilder entityBuilder,  Repository repo ) {
    super(entityBuilder,  repo );
  }
    
    @Override
    public com.isw.svcord21.sdk.domain.svcord.entity.Svcord21 createServicingOrderProducer(com.isw.svcord21.sdk.domain.svcord.entity.CreateServicingOrderProducerInput createServicingOrderProducerInput)  {
    
      log.info("Svcord21Command.createServicingOrderProducer()");
      // TODO: Add your command implementation logic

      Svcord21Entity servicingOrderProcedure = this.entityBuilder.getSvcord().getSvcord21().build();
      servicingOrderProcedure.setCustomerReference(createServicingOrderProducerInput.getCustomerReference());
      servicingOrderProcedure.setProcessStartDate(LocalDate.now());
      servicingOrderProcedure.setServicingOrderType(ServicingOrderType.valueOf("PAYMENT_CASH_WITHDRAWALS"));
      servicingOrderProcedure.setServicingOrderWorkDescription("CASH WITHDRAWALS");
      servicingOrderProcedure.setServicingOrderWorkProduct(ServicingOrderWorkProduct.valueOf("PAYMENT"));
      servicingOrderProcedure.setServicingOrderWorkResult(ServicingOrderWorkResult.valueOf("PROCESSING"));

      ThirdPartyReferenceEntity thirdPartyReference = new ThirdPartyReferenceEntity();
      thirdPartyReference.setId("test1");
      thirdPartyReference.setPassword("password");
      servicingOrderProcedure.setThirdPartyReference(thirdPartyReference);

      return repo.getSvcord().getSvcord21().save(servicingOrderProcedure);
    }
  
    
    @Override
    public void updateServicingOrderProducer(com.isw.svcord21.sdk.domain.svcord.entity.Svcord21 instance, com.isw.svcord21.sdk.domain.svcord.entity.UpdateServicingOrderProducerInput updateServicingOrderProducerInput)  { 

      log.info("Svcord21Command.updateServicingOrderProducer()");
      // TODO: Add your command implementation logic

      Svcord21Entity servicingOrderProcedure = this.repo.getSvcord().getSvcord21().getReferenceById(Long.parseLong(updateServicingOrderProducerInput.getUpdateID()));
      servicingOrderProcedure.setCustomerReference(updateServicingOrderProducerInput.getCustomerReference());
      servicingOrderProcedure.setProcessEndDate(LocalDate.now());
      servicingOrderProcedure.setServicingOrderWorkResult(updateServicingOrderProducerInput.getServicingOrderWorkResult());
      servicingOrderProcedure.setThirdPartyReference(updateServicingOrderProducerInput.getThirdPartyReference());

      log.info(updateServicingOrderProducerInput.getUpdateID().toString());
      log.info(updateServicingOrderProducerInput.getCustomerReference().getAccountNumber());
      log.info(updateServicingOrderProducerInput.getCustomerReference().getAmount());

      this.repo.getSvcord().getSvcord21().save(servicingOrderProcedure);
    }
  
}
